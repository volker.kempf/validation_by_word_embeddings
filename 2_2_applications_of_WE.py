#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 11:46:08 2022

@author: Volker Kempf
"""
# %%
import data_loading as DL
import WE_functions as WEf
import numpy as np

# %%
if __name__ == '__main__':
    # Load fTWiki model; takes some time
    model = DL.load_model('fasttext-wiki-news-subwords-300')
    model.init_sims(replace=True)

    # Define vocabulary for tests
    vocab = ['ant', 'termite', 'crab', 'shrimp', 'emu']
    analogies = [['actor', 'actress', 'waiter'], ['white', 'black', 'up'], ['tree', 'leaf', 'flower'], ['dog', 'puppy', 'cat'], ['pork', 'pig', 'beef'], ['cold', 'colder', 'small'], ['emu','bird','ant']]

    # %% Run tests for Section 2.2.3

    # Closest words to given word by cosine similarity and distances to other vocabulary words
    for word in ['ant', 'crab']:
        print(WEf.get_closest_words(model, word, 5, dist_type='cosine'), )
        print([(word2, WEf.word_dist(model[word], model[word2], 'cosine')) for word2 in vocab], '\n')

    # Word analogies a to b is like c to x
    for words in analogies:
        print(words[0]+' is to '+words[1]+' like '+words[2]+' is to '+WEf.get_analogous_words(model, words[0], words[1], words[2], 'cosine'))

    # %% Run tests for Section 2.2.4
    # Load fTCrawl model; takes some time
    model = DL.load_fastText('fastText.crawl', 300)
    model.init_sims(replace=True)

    # Define vocabulary for tests
    vocab2 = ['Easter','Christmas','Actress','King']
    opposites = [('Spring','Winter'),('Weak','Strong'),('Man','Woman')]

    # Compute POLAR coordinates
    dimensions = np.linalg.pinv(np.array([(model[opposite[0]]-model[opposite[1]]) for opposite in opposites]).T)
    vocab2_vecs = np.array([model[word] for word in vocab2])
    print(dimensions.dot(vocab2_vecs.T).T)