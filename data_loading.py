import numpy as np
import gensim.downloader as api
import WE_functions as WEf
import csv
from gensim.test.utils import datapath
from gensim.models import KeyedVectors, fasttext


def load_ipip(file_name):
    traits2facets_dict = {'extraversion': ['extraversion', 'sociability', 'gregariousness', 'enthusiasm', 'assertiveness', 'activity', 'adventurousness'],\
                   'agreeableness': ['agreeableness', 'compassion', 'altruism', 'understanding', 'warmth', 'respectfulness', 'compliance', 'gentleness', 'politeness', 'trust', 'pleasantness', 'friendliness'],\
                   'conscientiousness': ['conscientiousness', 'organization', 'order', 'orderliness', 'productiveness', 'self-discipline', 'efficiency', 'industriousness', 'responsibility', 'dutifulness', 'reliability', 'industriousness/perseverance/persistence'],\
                   'neuroticism': ['neuroticism', 'anxiety', 'emotionality', 'withdrawal', 'depression', 'insecurity', 'emotional volatility', 'angry hostility', 'irritability', 'volatility', 'anger'],\
                   'open-mindedness': ['openness', 'intellectual curiosity', 'ideas', 'intellect', 'aesthetic sensitivity', 'aesthetics', 'reflection', 'creativity', 'imagination', 'fantasy', 'ingenuity', 'openness to experience', 'aesthetic appreciation/artistic interests', 'creativity/originality', 'intellectual openness']}
    facets2traits_dict = {facet: trait for trait, facets in traits2facets_dict.items() for facet in facets}

    items = dict()
    with open('./data/'+file_name, 'r') as file_in:
        reader = csv.reader(file_in, delimiter=';')
        next(reader)
        for line in reader:
            item = line[3].lower()
            facet = line[4].lower()
            try:
                trait = facets2traits_dict[facet]
                items[line[3]] = (trait, line[2])
            except KeyError:
                continue
    items_2 = dict()
    for item, tup in items.items():
        if not tup[0] in items_2:
            items_2[tup[0]] = dict()
        for pn in ['positive', 'negative']:
            if not pn in items_2[tup[0]]:
                items_2[tup[0]][pn] = list()
        if tup[1] == '1':
            items_2[tup[0]]['positive'].append(item)
        elif tup[1] == '-1':
            items_2[tup[0]]['negative'].append(item)
    return items_2


def load_inventory(file_name):
    inventory_dict = dict()
    with open('./data/'+file_name, 'r') as file_in:
        for line in file_in:
            line = line.rstrip('\n').split(None, 2)
            if not line[0] in inventory_dict:
                inventory_dict[line[0]] = dict()
                inventory_dict[line[0]]['positive'] = list()
                inventory_dict[line[0]]['negative'] = list()
            if int(line[1]) == 0:
                inventory_dict[line[0]]['positive'].append(line[2].lower())
            else:
                inventory_dict[line[0]]['negative'].append(line[2].lower())
    return inventory_dict


def load_trait_poles(model, filename):
    trait_poles = dict()
    trait_adj = dict()
    with open(filename, 'r') as file_in:
        for line in file_in:
            line = line.rstrip('\n').split(' ')
            if not line[0] in trait_poles:
                trait_poles[line[0]] = dict()
                trait_adj[line[0]] = dict()
            trait_adj[line[0]][line[1]] = line[2:]
            pole = list()
            for word in line[2:]:
                pole.append(WEf.get_item_embedding(model, word, False))
            trait_poles[line[0]][line[1]] = sum(pole) / np.linalg.norm(sum(pole))
    trait_dimensions = dict()
    for trait, pos_neg_dict in trait_poles.items():
        trait_dimensions[trait] = (pos_neg_dict['positive'] - pos_neg_dict['negative'])
    return trait_poles, trait_adj, trait_dimensions


def load_glove(version, dim):
    return KeyedVectors.load_word2vec_format(datapath('/home/volker/Documents/validation_by_word_embeddings/'+version+'/'+version+'.'+str(dim)+'d.txt'), binary=False, no_header=True)


def load_fastText(version, dim):
    try:
        return fasttext.load_facebook_model('./'+version+'/'+version+'.'+str(dim)+'d.bin').wv
    except:
        return KeyedVectors.load_word2vec_format(datapath('/hdd/validation_by_word_embeddings/'+version+'/'+version+'.'+str(dim)+'d.vec'), binary=False, no_header=False)


def load_model(model_name):
    return api.load(model_name)