import numpy as np
import itertools
from gensim.parsing.preprocessing import remove_stopwords, strip_punctuation, strip_multiple_whitespaces
from sklearn import metrics
from sklearn.cluster import KMeans
import results_export as RD


def word_dist(a, b, dist_type='cosine'):
    """
    Parameters
    ----------
    a : numpy array
    b : numpy array
    dist_type : str, euclidian or cosine, the default is 'cosine'.

    Returns
    -------
    distance between vectors
    """
    if dist_type == 'cosine':
        return np.dot(a,b)/(np.linalg.norm(a)*np.linalg.norm(b))
    elif dist_type == 'euclidian':
        return np.linalg.norm(a-b)


def get_item_embedding(model, item: str, normalized=True, filter_stop_words=False):
    """
    Parameters
    ----------
    model : Word embedding model, of type gensim.model.keyedvectors
    item : str, can be multiple words
    normalized : bool, the default is True.
    filter_stop_words : bool, default is False

    Returns
    -------
    embedding vector for item, averaged if multiple words
    """
    item = strip_multiple_whitespaces(strip_punctuation(item))
    if filter_stop_words:
        item = remove_stopwords(item)
    item = item.split(None)

    item_embedding = list()
    for word in item:
        try:
            item_embedding.append(model[word])
        except:
            print('No word vector:', word)

    if len(item_embedding) == 0:
        return np.zeros(len(model['the']))

    item_embedding_sum = sum(item_embedding)
    if normalized:
        return np.array(item_embedding_sum / np.linalg.norm(item_embedding_sum))
    else:
        return np.array(item_embedding_sum / len(item_embedding))


def get_closest_words(model, word, N: int, dist_type: str):
    """
    Parameters
    ----------
    model : Word embedding model, gensim.model.keyedvectors
    word : str or vector
    N : int, number of return closest words
    dist_type : type of distance to use, supported values cosine and euclidian

    Returns
    -------
    list of tuples (word, distance) of closest words, sorted by distances
    """
    if type(word) == str:
        word_v = get_item_embedding(model, word, False)
    if dist_type == 'cosine':
        return sorted(model.most_similar(positive=[word], topn=N), key=lambda tup: tup[1], reverse=True)[:N]
    elif dist_type == 'euclidian':
        return sorted([(test_word, word_dist(word_v, model[test_word], 'euclidian')) for test_word in model.index_to_key], key=lambda tup: tup[1], reverse=False)[:N]


def get_analogous_words(model, a: str, b: str, c: str, dist_type):
    """
    Parameters
    ----------
    model : Word embedding model, gensim.model.keyedvectors
    a : str
    b : str
    c : str
    dist_type : str, type of distance function, euclidian or cosine

    Returns
    -------
    a is to b as c is to return
    """
    analogs = get_closest_words(model, get_item_embedding(model, c, False)+get_item_embedding(model, b, False)-get_item_embedding(model, a, False), 4, dist_type)
    analogs = [analog[0] for analog in analogs if not analog[0] in [a,b,c]]
    return analogs[0]


def get_vectors_words_labels(model, words, which='positive'):
    """
    Parameters
    ----------
    model : Word embedding model, gensim.model.keyedvectors
    words : dict of words or items

    Returns
    -------
    vectors : list of word vectors
    adjectives : dictionary with entries "word: trait"
    labels_true : list with true cluster labels
    """
    vectors = list()
    labels_true = list()
    adjectives = dict()
    for m, trait in enumerate(words.keys()):
        for adj in words[trait][which]:
            vec = get_item_embedding(model, adj, True)
            if not abs(np.linalg.norm(vec)) < 1e-6:
                vectors.append(vec)
                labels_true.append(m)
                adjectives[adj] = trait
    return vectors, adjectives, labels_true


def measures_labeling(labels_true, labels_alg):
    return {'Rand Score': metrics.rand_score(labels_true, labels_alg),\
            'Adjusted Rand Score': metrics.adjusted_rand_score(labels_true, labels_alg),\
            'Mutual Information': metrics.mutual_info_score(labels_true, labels_alg),\
            'Adjusted Mutual Information': metrics.adjusted_mutual_info_score(labels_true, labels_alg),\
            'Homogeneity': metrics.homogeneity_score(labels_true, labels_alg),\
            'Completeness': metrics.completeness_score(labels_true, labels_alg),\
            'V-Measure': metrics.v_measure_score(labels_true, labels_alg),\
            'Fowlkes Mallows Score': metrics.fowlkes_mallows_score(labels_true, labels_alg)}


def cluster_adjectives(model, trait_adj, n_clusters, n_runs, MS_type='Adjusted Mutual Information', export=False):
    """
    Parameters
    ----------
    model : Word embedding model, gensim.model.keyedvectors
    trait_adj : adjectives to be clustered
    n_clusters : number of clusters
    n_runs : number of runs with random initialization, if 0, all possible initializations are computed sequentially
    MS_type : which of the clustering scores is maximized over the runs, default is 'Adjusted Mutual Information'
    """
    def run_clustering(init_conf, n_init, RS_old, clustering_KMeans, scores_KMeans, MS_type):
        clustering = KMeans(n_clusters=n_clusters, init=init_conf, algorithm='full', n_init=n_init).fit(vectors)
        scores = measures_labeling(labels_true, clustering.labels_)
        if scores[MS_type] > RS_old:
            clustering_KMeans = clustering
            scores_KMeans = scores
            RS_old = scores[MS_type]
        return scores_KMeans, RS_old, clustering_KMeans

    vectors, adjectives, labels_true = get_vectors_words_labels(model, trait_adj)

    RS_old = 0; clustering_KMeans = None; scores_KMeans = None
    if n_runs == 0:
        for i, comb in enumerate(itertools.combinations(list(range(len(vectors))), n_clusters)):
            scores_KMeans, RS_old, clustering_KMeans = run_clustering(np.array([vectors[k] for k in comb]), 1, RS_old, clustering_KMeans, scores_KMeans, MS_type)
            print('\r Progress: '+"{0:.1%}".format((i+1)*np.math.factorial(n_clusters)*np.math.factorial(len(vectors)-n_clusters)/(np.math.factorial(len(vectors))))+' Current best: '+str(RS_old), end=' ')
    else:
        for i in range(n_runs):
            scores_KMeans, RS_old, clustering_KMeans = run_clustering('random', 5, RS_old, clustering_KMeans, scores_KMeans, MS_type)
            print('\r Progress: '+"{0:.1%}".format((i+1)/n_runs)+' Current best: '+str(RS_old), end=' ')
    print('\nKMeans:', scores_KMeans)
    print(len(labels_true), labels_true)
    print(len(clustering_KMeans.labels_), clustering_KMeans.labels_)
    if export:
        RD.clusters2tikz(clustering_KMeans.labels_, labels_true, adjectives, 'clustersKMeans.txt')