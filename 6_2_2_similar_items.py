#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 15:08:27 2022

@author: Volker Kempf
"""
# %%
import data_loading as DL
import WE_functions as WEf
import itertools


def check_item_similarity(model, inventory, threshold=0.726, filter_stop_words=False):
    similar_items = dict()
    for trait, pn_dict in inventory.items():
        similar_items[trait] = dict()
        for pn in ['positive', 'negative']:
            similar_items[trait][pn] = list()
            items = list(pn_dict[pn])
            for tup in itertools.combinations(items, 2):
                sim = WEf.word_dist(WEf.get_item_embedding(model, tup[0], filter_stop_words=filter_stop_words), WEf.get_item_embedding(model, tup[1], filter_stop_words=filter_stop_words))
                if sim > threshold:
                    print(tup, sim)
                    similar_items[trait][pn].append((tup[0], tup[1], sim))
    print(similar_items)


# %%
if __name__ == '__main__':
    # Load word embedding model; takes some time; uncomment the desired model

    # model = DL.load_fastText('fastText.crawl', 300)           # fTCrawl
    # model = DL.load_model('fasttext-wiki-news-subwords-300')  # fTWiki
    # model = DL.load_glove('glove.840B',300)                   # GVCrawl
    # model = DL.load_glove('glove.twitter.27B',200)            # GVTwitter
    model = DL.load_glove('glove.6B',300)                     # GVWiki
    # model = DL.load_model('word2vec-google-news-300')         # w2vNews

    # Normalize all vectors
    model.init_sims(replace=True)
    # %% load data, uncomment the desired inventory

    # inventory = DL.load_inventory('BFI_items_raw.dat')
    inventory = DL.load_inventory('BFI2_items_raw.dat')

    # %% Run evaluation; threshold 0.726 for dim=300, 0.767 for dim=200
    check_item_similarity(model, inventory, threshold=0.726, filter_stop_words=True)
