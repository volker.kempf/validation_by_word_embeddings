\begin{axis}[
	xmin=-0.44690796001759303,
	xmax=0.5207569847152461,
	ymin=-0.3535369321780832,
	ymax=0.5175878012127111,
	width=\textwidth,
	clip=false,
	legend style={font=\tiny, anchor=north east, at={(1,1)}},
	legend cell align=left,
	height=0.8\textwidth,
	zmin=-0.3890305750179402,
	zmax=0.460620951151338,
	view={80}{40},
]
	\draw[densely dashed, wbhblue] (0.16424077629277112,-0.3139403533875925,0.17381615408999204) -- (0.16424077629277112,-0.3139403533875925,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhblue] (0.004511355602460852,-0.1043594245196281,0.12910682174277288) -- (0.004511355602460852,-0.1043594245196281,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhblue] (-0.2128779112562754,0.1862390638812376,0.21775125167374954) -- (-0.2128779112562754,0.1862390638812376,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhblue] (-0.127718335675083,-0.18430680643054548,0.0699401804951652) -- (-0.127718335675083,-0.18430680643054548,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhblue] (-0.04906061026603475,-0.29216597868195104,0.3970004272345526) -- (-0.04906061026603475,-0.29216597868195104,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhpurple] (0.03198601978814944,-0.0941970714188602,-0.14528039509443863) -- (0.03198601978814944,-0.0941970714188602,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhpurple] (0.009843490592341003,0.0775702080034353,-0.169427938407263) -- (0.009843490592341003,0.0775702080034353,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhpurple] (0.04202691874975655,-0.20786324114652832,-0.2796354141838231) -- (0.04202691874975655,-0.20786324114652832,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhpurple] (0.024857920284450514,-0.25883308831889057,-0.021910177895902203) -- (0.024857920284450514,-0.25883308831889057,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhpurple] (-0.03902344899556947,-0.09629285590921834,0.14362998479436198) -- (-0.03902344899556947,-0.09629285590921834,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhgreen] (-0.2499180128590465,0.4779912224222205,0.2548595137937963) -- (-0.2499180128590465,0.4779912224222205,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhgreen] (-0.24300703578963173,0.09693741834576396,-0.3252900848572559) -- (-0.24300703578963173,0.09693741834576396,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhgreen] (-0.06951996213563061,0.34275092779987487,-0.11897931115808544) -- (-0.06951996213563061,0.34275092779987487,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhgreen] (-0.40292318980246394,0.06013022070466015,-0.04382530556428552) -- (-0.40292318980246394,0.06013022070466015,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhgreen] (-0.21141269084280617,0.2856715041434736,-0.02097983128256682) -- (-0.21141269084280617,0.2856715041434736,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhdarkpurple] (0.3942756379101022,0.19955997825777244,0.22036830976343835) -- (0.3942756379101022,0.19955997825777244,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhdarkpurple] (0.45353992862330766,0.06726420941781969,-0.044213019089538064) -- (0.45353992862330766,0.06726420941781969,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhdarkpurple] (0.3726954563462769,0.10585462340218874,0.023962150367653777) -- (0.3726954563462769,0.10585462340218874,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhdarkpurple] (0.30009525180633706,-0.18659911817761055,0.16780698811633324) -- (0.30009525180633706,-0.18659911817761055,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhdarkpurple] (0.4767722145001171,0.25066446202317294,-0.2795391272623689) -- (0.4767722145001171,0.25066446202317294,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhred] (-0.2219299562170147,-0.17722663988708143,-0.15814626278713081) -- (-0.2219299562170147,-0.17722663988708143,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhred] (-0.2855185177004211,-0.2039329324950551,-0.07338556548716012) -- (-0.2855185177004211,-0.2039329324950551,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhred] (-0.22295837416224845,0.19684088144506925,-0.008812461965614776) -- (-0.22295837416224845,0.19684088144506925,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhred] (-0.22469213645325797,-0.23154600395397673,-0.3584068359352279) -- (-0.22469213645325797,-0.23154600395397673,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\draw[densely dashed, wbhred] (0.28571521165941344,0.0037887944802495644,-0.37541005110115483) -- (0.28571521165941344,0.0037887944802495644,-0.3504100511011548) node[cross out , inner sep=0pt, minimum size=1pt, draw] {};
	\node[wbhblue, font=\tiny\bfseries] at (0.16424077629277112,-0.3139403533875925,0.19881615408999204) {talkative};
	\node[wbhblue, font=\tiny\bfseries] at (0.004511355602460852,-0.1043594245196281,0.15410682174277288) {assertive};
	\node[wbhblue, font=\tiny\bfseries] at (-0.2128779112562754,0.1862390638812376,0.24275125167374953) {active};
	\node[wbhblue, font=\tiny\bfseries] at (-0.127718335675083,-0.18430680643054548,0.09494018049516521) {energetic};
	\node[wbhblue, font=\tiny\bfseries] at (-0.04906061026603475,-0.29216597868195104,0.4220004272345526) {outgoing};
	\node[wbhpurple, font=\tiny\bfseries] at (0.03198601978814944,-0.0941970714188602,-0.12028039509443862) {sympathetic};
	\node[wbhpurple, font=\tiny\bfseries] at (0.009843490592341003,0.0775702080034353,-0.144427938407263) {kind};
	\node[wbhpurple, font=\tiny\bfseries] at (0.04202691874975655,-0.20786324114652832,-0.25463541418382307) {appreciative};
	\node[wbhpurple, font=\tiny\bfseries] at (0.024857920284450514,-0.25883308831889057,0.003089822104097798) {affectionate};
	\node[wbhpurple, font=\tiny\bfseries] at (-0.03902344899556947,-0.09629285590921834,0.16862998479436198) {soft-hearted};
	\node[wbhgreen, font=\tiny\bfseries] at (-0.2499180128590465,0.4779912224222205,0.2798595137937963) {organized};
	\node[wbhgreen, font=\tiny\bfseries] at (-0.24300703578963173,0.09693741834576396,-0.3002900848572559) {thorough};
	\node[wbhgreen, font=\tiny\bfseries] at (-0.06951996213563061,0.34275092779987487,-0.09397931115808543) {planful};
	\node[wbhgreen, font=\tiny\bfseries] at (-0.40292318980246394,0.06013022070466015,-0.01882530556428552) {efficient};
	\node[wbhgreen, font=\tiny\bfseries] at (-0.21141269084280617,0.2856715041434736,0.004020168717433178) {responsible};
	\node[wbhdarkpurple, font=\tiny\bfseries] at (0.3942756379101022,0.19955997825777244,0.24536830976343835) {tense};
	\node[wbhdarkpurple, font=\tiny\bfseries] at (0.45353992862330766,0.06726420941781969,-0.01921301908953806) {anxious};
	\node[wbhdarkpurple, font=\tiny\bfseries] at (0.3726954563462769,0.10585462340218874,0.04896215036765378) {nervous};
	\node[wbhdarkpurple, font=\tiny\bfseries] at (0.30009525180633706,-0.18659911817761055,0.19280698811633323) {moody};
	\node[wbhdarkpurple, font=\tiny\bfseries] at (0.4767722145001171,0.25066446202317294,-0.2545391272623689) {worrying};
	\node[wbhred, font=\tiny\bfseries] at (-0.2219299562170147,-0.17722663988708143,-0.13314626278713082) {imaginative};
	\node[wbhred, font=\tiny\bfseries] at (-0.2855185177004211,-0.2039329324950551,-0.048385565487160116) {intelligent};
	\node[wbhred, font=\tiny\bfseries] at (-0.22295837416224845,0.19684088144506925,0.016187538034385225) {original};
	\node[wbhred, font=\tiny\bfseries] at (-0.22469213645325797,-0.23154600395397673,-0.33340683593522785) {insightful};
	\node[wbhred, font=\tiny\bfseries] at (0.28571521165941344,0.0037887944802495644,-0.3504100511011548) {curious};
	\addlegendimage{only marks, mark=square*, wbhblue}
	\addlegendentry{extraversion}
	\addlegendimage{only marks, mark=square*, wbhpurple}
	\addlegendentry{agreeableness}
	\addlegendimage{only marks, mark=square*, wbhgreen}
	\addlegendentry{conscientiousness}
	\addlegendimage{only marks, mark=square*, wbhdarkpurple}
	\addlegendentry{neuroticism}
	\addlegendimage{only marks, mark=square*, wbhred}
	\addlegendentry{open-mindedness}
\end{axis}