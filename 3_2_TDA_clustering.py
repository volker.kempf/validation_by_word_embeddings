#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 14:18:49 2022

@author: Volker Kempf
"""
# %%
import data_loading as DL
import WE_functions as WEf
import results_export as RD
from sklearn.decomposition import PCA

# %%
if __name__ == '__main__':
    # Load word embedding model; takes some time; uncomment the desired model

    # model = DL.load_fastText('fastText.crawl', 300)           # fTCrawl
    # model = DL.load_model('fasttext-wiki-news-subwords-300')  # fTWiki
    # model = DL.load_glove('glove.840B',300)                   # GVCrawl
    # model = DL.load_glove('glove.twitter.27B',200)            # GVTwitter
    model = DL.load_glove('glove.6B',300)                     # GVWiki
    # model = DL.load_model('word2vec-google-news-300')         # w2vNews

    # %% load data
    # trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Joh21_top10.dat')
    # trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Joh21_top8.dat')
    trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Joh21_top5.dat')

    # %% Compute PCA and export it to tikz for vizualization
    dims = 3
    vectors, adjectives, _ = WEf.get_vectors_words_labels(model, trait_adj)
    reduced_pca = PCA(n_components=dims, svd_solver='full').fit_transform(vectors)
    RD.PCAPlot2tikz(reduced_pca, adjectives, 'PCA'+str(dims)+'.txt')

    # %% Compute clusterings
    n_runs = 10 # 0 means all combinations
    WEf.cluster_adjectives(model, trait_adj, 5, n_runs, MS_type='Adjusted Mutual Information')