#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 14:58:15 2022

@author: Volker Kempf
"""
# %%
import data_loading as DL
import WE_functions as WEf
from sklearn import metrics
import tabulate


def compute_nearest_traits(model, inventory, trait_poles, only_pos=True, filter_stop_words=False):
    labels_true = list()
    labels_nearest = list()
    labels = ['extraversion', 'agreeableness', 'conscientiousness', 'neuroticism', 'open-mindedness']
    for trait, pn_dict in inventory.items():
        for pn, items in pn_dict.items():
            if only_pos and pn=='negative':
                continue
            table = list()
            for item in items:
                dists = list()
                for tr in inventory.keys():
                    if pn == 'positive':
                        dists.append((tr, WEf.word_dist(trait_poles[tr]['positive'],WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words))))
                    elif pn == 'negative' and not only_pos:
                        dists.append((tr+'', WEf.word_dist(trait_poles[tr]['negative'],WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words))))
                table.append([str(item)]+sorted(dists, key=lambda tup: tup[1], reverse=True)[:1])
                labels_true.append(trait)
                labels_nearest.append(sorted(dists, key=lambda tup: tup[1], reverse=True)[0][0])
            print('\n', tabulate.tabulate(table,headers=[trait+' '+pn[0], 'Nearest']), '\n')
    print(metrics.precision_recall_fscore_support(labels_true, labels_nearest, labels=labels)[2])
    print(metrics.precision_recall_fscore_support(labels_true, labels_nearest, average='weighted')[2])
    print(metrics.confusion_matrix(labels_true, labels_nearest, labels=labels))


# %%
if __name__ == '__main__':
    # Load word embedding model; takes some time; uncomment the desired model

    # model = DL.load_fastText('fastText.crawl', 300)           # fTCrawl
    # model = DL.load_model('fasttext-wiki-news-subwords-300')  # fTWiki
    # model = DL.load_glove('glove.840B',300)                   # GVCrawl
    # model = DL.load_glove('glove.twitter.27B',200)            # GVTwitter
    model = DL.load_glove('glove.6B',300)                     # GVWiki
    # model = DL.load_model('word2vec-google-news-300')         # w2vNews

    # %% load data, uncomment the desired inventory
    trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Joh21_top10.dat')

    inventory = DL.load_inventory('BFI_items_raw.dat')
    # inventory = DL.load_inventory('BFI2_items_raw.dat')
    # inventory = DL.load_inventory('BFI_items_adj_interpreted.dat')
    # inventory = DL.load_inventory('BFI2_items_adj_interpreted.dat')

    ipip = DL.load_ipip('IPIP.csv')

    # %% Run evaluation; Set filter_stop_words to False for I1 stage, True for I2 stage; for I3 stage load separate inventory
    compute_nearest_traits(model, inventory, trait_poles, only_pos=True, filter_stop_words=True)
    # compute_nearest_traits(model, ipip, trait_poles, only_pos=True, filter_stop_words=True)