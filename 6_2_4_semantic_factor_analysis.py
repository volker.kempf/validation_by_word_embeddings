#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 15:21:47 2022

@author: Volker Kempf
"""
# %%
import data_loading as DL
import results_export as RD
import WE_functions as WEf
import numpy as np


def project2traitdimensions(model, inventory, trait_dimensions, trait_poles, filter_stop_words=True):
    trafo_matrix = np.array([x for x in trait_dimensions.values()])
    pole_matrix = trafo_matrix.dot(np.array([vec for pn_dict in trait_poles.values() for vec in pn_dict.values()]).T).T
    normalization_matrix = np.array([[2/(pole_matrix[2*i][j]-pole_matrix[2*i+1][j]) if i==j else 0 for j in range(5)] for i in range(5)])
    normalization_bias = np.array([1-2*pole_matrix[2*i][i]/(pole_matrix[2*i][i]-pole_matrix[2*i+1][i]) for i in range(5)])
    inventory_trafo = dict()
    for trait, pn_dict in inventory.items():
        inventory_trafo[trait] = dict()
        for pn, items in pn_dict.items():
            avg = None
            inventory_trafo[trait][pn] = dict()
            for item in items:
                inventory_trafo[trait][pn][item] = trafo_matrix.dot(np.array(WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words)))
                if not avg is None:
                    avg +=  trafo_matrix.dot(np.array(WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words)))
                else:
                    avg =  trafo_matrix.dot(np.array(WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words)))
            avg = avg/len(items)
            avg_normalized = avg.dot(normalization_matrix) + normalization_bias
            print(trait, pn, avg, avg.dot(normalization_matrix) + normalization_bias)
            RD.vectorsPOLAR2tikz(list(inventory.keys()), avg, avg_normalized, trait, pn, 'POLAR_test.txt')
    return inventory_trafo, avg, avg_normalized


def project2POLARtraitdimensions(model, inventory, trait_dimensions, trait_poles, filter_stop_words=True):
    trafo_matrix = np.linalg.pinv(np.array([x for x in trait_dimensions.values()]).T)
    pole_matrix = trafo_matrix.dot(np.array([vec for pn_dict in trait_poles.values() for vec in pn_dict.values()]).T).T
    normalization_matrix = np.array([[2/(pole_matrix[2*i][j]-pole_matrix[2*i+1][j]) if i==j else 0 for j in range(5)] for i in range(5)])
    normalization_bias = np.array([1-2*pole_matrix[2*i][i]/(pole_matrix[2*i][i]-pole_matrix[2*i+1][i]) for i in range(5)])
    inventory_trafo = dict()
    avg_dict = dict()
    SD_dict = dict()
    for trait, pn_dict in inventory.items():
        inventory_trafo[trait] = dict()
        avg_dict[trait] = dict()
        SD_dict[trait] = dict()
        for pn, items in pn_dict.items():
            avg = None
            inventory_trafo[trait][pn] = dict()
            for item in items:
                inventory_trafo[trait][pn][item] = trafo_matrix.dot(np.array(WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words)))
                if not avg is None:
                    avg +=  trafo_matrix.dot(np.array(WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words)))
                else:
                    avg =  trafo_matrix.dot(np.array(WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words)))
            avg = avg/len(items)
            avg_normalized = avg.dot(normalization_matrix) + normalization_bias
            SD = None
            for item in items:
                item_normalized = inventory_trafo[trait][pn][item].dot(normalization_matrix) + normalization_bias
                if SD is None:
                    SD = np.square(item_normalized-avg_normalized)
                else:
                    SD += np.square(item_normalized-avg_normalized)
            SD = np.sqrt(SD/len(items))
            print(trait, pn, avg, avg_normalized, SD)
            avg_dict[trait][pn] = avg_normalized
            SD_dict[trait][pn] = SD
    RD.vectorsPOLAR2tikz(list(inventory.keys()), avg_dict, SD_dict, 'POLAR_test.txt')
    return inventory_trafo


# %%
if __name__ == '__main__':
    # Load word embedding model; takes some time; uncomment the desired model

    # model = DL.load_fastText('fastText.crawl', 300)           # fTCrawl
    # model = DL.load_model('fasttext-wiki-news-subwords-300')  # fTWiki
    # model = DL.load_glove('glove.840B',300)                   # GVCrawl
    model = DL.load_glove('glove.twitter.27B',200)            # GVTwitter
    # model = DL.load_glove('glove.6B',300)                     # GVWiki
    # model = DL.load_model('word2vec-google-news-300')         # w2vNews

    # %% load data, uncomment the desired inventory
    trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Joh21_top10.dat')
    # inventory = DL.load_inventory('BFI_items_raw.dat')
    inventory = DL.load_inventory('BFI2_items_raw.dat')

    # %% Run evaluation
    project2POLARtraitdimensions(model, inventory, trait_dimensions, trait_poles, filter_stop_words=True)