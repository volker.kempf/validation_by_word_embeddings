#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 13:31:38 2022

@author: Volker Kempf
"""
# %%
import data_loading as DL
import WE_functions as WEf
import numpy as np
import tabulate

# %%
if __name__ == '__main__':
    # Load word embedding model; takes some time; uncomment the desired model

    # model = DL.load_fastText('fastText.crawl', 300)           # fTCrawl
    # model = DL.load_model('fasttext-wiki-news-subwords-300')  # fTWiki
    # model = DL.load_glove('glove.840B',300)                   # GVCrawl
    model = DL.load_glove('glove.twitter.27B',200)            # GVTwitter
    # model = DL.load_glove('glove.6B',300)                     # GVWiki
    # model = DL.load_model('word2vec-google-news-300')         # w2vNews

    # %% Load desired trait adjectives from Gol92 or Joh21
    trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Gol92.dat')
    # trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Joh21_top10.dat')

    # %% Run tests from Section 3.1
    angles = list()
    for trait, vec in trait_dimensions.items():
        cos_sims = [round(np.arccos(min(1,WEf.word_dist(vec, vec2, 'cosine')))*360/(2*np.pi), 0) for vec2 in trait_dimensions.values()]
        angles.append([str(trait)]+cos_sims)
    mean = sum([sum(angles[i][i+2:]) for i in range(5)])/10
    sd = np.sqrt(sum([(angles[i][j]-mean)**2 for i in range(5) for j in range(i+2,6)])/10)
    print(tabulate.tabulate(angles,headers=list(trait_dimensions.keys())), '\n')
    print('Mean:', mean, 'SD:', sd, '\n')