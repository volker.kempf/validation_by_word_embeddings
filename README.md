# Validation of personality survey intruments using vector representations of natural language
## Prerequisites
The scripts use functionality of the following python modules:
* gensim
* scikit-learn
* tabulate
* itertools
* numpy

Some of the pre-trained word embedding data needs to be downloaded separately and placed in the correct folders:
* ./fastText.crawl/fastText.crawl.300d.bin downloaded from https://dl.fbaipublicfiles.com/fasttext/vectors-english/crawl-300d-2M-subword.zip
* ./GloVe.6B/glove.6B.300d.txt downloaded from https://nlp.stanford.edu/data/glove.6B.zip
* ./GloVe.twitter.27B/glove.twitter.27B.200d.txt downloaded from https://nlp.stanford.edu/data/glove.twitter.27B.zip
* ./GloVe.840B/glove.840B.300d.txt downloaded from https://nlp.stanford.edu/data/glove.840B.300d.zip

## Authors
Volker Kempf

## License
GPL-3.0-or-later
