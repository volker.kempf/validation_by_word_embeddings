#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 15:18:03 2022

@author: Volker Kempf
"""
# %%
import data_loading as DL
import WE_functions as WEf
from sklearn import preprocessing


def check_meanSD_overlap(model, trait_adj, inventory, filter_stop_words=True, normalized=True):
    pole_meanSD_dict = dict()
    for trait, pn_dict in trait_adj.items():
        pole_meanSD_dict[trait] = dict()
        for pn, adjs in pn_dict.items():
            pole_meanSD_dict[trait][pn] = dict()
            M = [WEf.get_item_embedding(model, adj, normalized=normalized) for adj in adjs]
            scaler = preprocessing.StandardScaler().fit(M)
            pole_meanSD_dict[trait][pn]['mean'] = scaler.mean_
            pole_meanSD_dict[trait][pn]['SD'] = scaler.scale_
    inventory_meanSD_dict = dict()
    for trait, pn_dict in inventory.items():
        inventory_meanSD_dict[trait] = dict()
        for pn, items in pn_dict.items():
            inventory_meanSD_dict[trait][pn] = dict()
            M = [WEf.get_item_embedding(model, item, filter_stop_words=filter_stop_words, normalized=normalized) for item in items]
            scaler = preprocessing.StandardScaler().fit(M)
            inventory_meanSD_dict[trait][pn]['mean'] = scaler.mean_
            inventory_meanSD_dict[trait][pn]['SD'] = scaler.scale_
    for trait, pn_dict in inventory_meanSD_dict.items():
        mean = 0
        for pn, meanSD_dict in pn_dict.items():
            dim = len(pole_meanSD_dict[trait][pn]['mean'])
            intervals_poles = [[pole_meanSD_dict[trait][pn]['mean'][i]-pole_meanSD_dict[trait][pn]['SD'][i], pole_meanSD_dict[trait][pn]['mean'][i]+pole_meanSD_dict[trait][pn]['SD'][i]] for i in range(dim)]
            intervals_inventory = [[inventory_meanSD_dict[trait][pn]['mean'][i]-inventory_meanSD_dict[trait][pn]['SD'][i], inventory_meanSD_dict[trait][pn]['mean'][i]+inventory_meanSD_dict[trait][pn]['SD'][i]] for i in range(dim)]
            overlap = 0
            for i in range(dim):
                s1 = intervals_poles[i][0]
                s2 = intervals_poles[i][1]
                t1 = intervals_inventory[i][0]
                t2 = intervals_inventory[i][1]
                intersection = min(s2,t2) - max(s1,t1)
                if intersection < 0 :
                    intersection = 0
                union = s2-s1 + t2-t1 - intersection
                overlap += intersection/union
            overlap = overlap/dim
            mean += overlap
            print(trait, pn, overlap)
        print(mean/2)


# %%
if __name__ == '__main__':
    # Load word embedding model; takes some time; uncomment the desired model

    # model = DL.load_fastText('fastText.crawl', 300)           # fTCrawl
    # model = DL.load_model('fasttext-wiki-news-subwords-300')  # fTWiki
    # model = DL.load_glove('glove.840B',300)                   # GVCrawl
    # model = DL.load_glove('glove.twitter.27B',200)            # GVTwitter
    # model = DL.load_glove('glove.6B',300)                     # GVWiki
    model = DL.load_model('word2vec-google-news-300')         # w2vNews

    # %% load data, uncomment the desired inventory
    trait_poles, trait_adj, trait_dimensions = DL.load_trait_poles(model, './data/trait_poles_Joh21_top10.dat')
    # inventory = DL.load_inventory('BFI_items_raw.dat')
    inventory = DL.load_inventory('BFI2_items_raw.dat')

    # %% Run evaluation
    check_meanSD_overlap(model, trait_adj, inventory, normalized=True)